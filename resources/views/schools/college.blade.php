@extends('layout.app')

@section('main-content')
{{ $schools->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
  <section id="content-custom">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <h1 class="mt-4"><h1></h1>
          <p class="lead">
            US DEGREE NETWORK is a complimentary service for finding colleges, career schools, and online degree programs.
          </p>
          <p>Please select from the <strong></strong> below to request complimentary information.</p>
          <div class="card my-4">
            <h5 class="card-header"><p>
            @if($schools->total() === 0 )
            <strong>No Search Result found.</strong>
            @else
            <strong>Search Result Returned {{ $schools->total() }} Schools:</strong>
            @endif
            </p>@include('layout.partials.paginate_head', ['result' => $schools])</h5>
            <div class="card-body">
              @foreach ($schools as $school)   
              <!-- start of loop for school name -->   
                <div class="row">
                  <div class="col-lg-10">
                    <a href="{{ URL::to('/students/form/' . $school->id)}}" target="_blank"><h4>{{$school->school_name}}</h4></a>
                  </div>
                  <div class="col-lg-2">
                    <a href="{{ URL::to('/students/form/' . $school->id)}}" target="_blank"><img class="logo-style" src="{{$school->images}}" alt="{{ $school->school_name }}"></a>
                  </div>
                </div>
                  <p><strong>Areas of study you may find at {{$school->school_name}}  :</strong></p>
                  <p>Courses</p>
                    @foreach ($school->courses as $course) 
                      <li>{{$course->course_name}}</li>
                    @endforeach
                  <p>Request More Information from <strong> <a href="{{ URL::to('/students/form/' . $school->id)}}" target="_blank">{{$school->school_name}}</strong></a></p>
                </hr>
                <hr>
              @endforeach
            </div>  
          </div>
          @include('layout.partials.paginate', ['result' => $schools]) <br>
        </div>
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
          @include('layout.partials.search-widget')
        </div>
      </div>
    </div>
  </section>
@endsection