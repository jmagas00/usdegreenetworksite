@extends('layout.app')

@section('main-content')
<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="bg-contact">
    <div class="carousel-inner" role="listbox">
      <div class="carousel-item active" style="background:url('img/contact-banner.jpg')">
        <div class="carousel-caption d-none d-md-block">
        </div>
      </div>          
    </div>
  </div>
  <section id="headcontent">
    <div class="container">
      <!-- <h4 style="text-align:left;"><strong>Contact Us</strong></h4>  -->
      <h1 style="text-align:left; color:#17a2b8; font-size:50px;">Contact Us</h1>
      <p style="text-align:left; color:#495057; font-size:20px;">Got a question? We'd love to hear from you.</p>
      <!-- <p style="text-align:left; collor">with over 1000 Colleges, Universities, and Career Schools in its database.</p>       -->
    </div>
  </section>
</header>

<section id="content-custom">
  <div class="container">
  <header class="business-header">
    <div class="container">
      
    </div>
  </header>
  <div class="contact-forms">
    <div class="row">
      <div class="col-md-8">
      <div class="container">
      <h1>Get in touch with us?</h1>
      <p>Send us a message and we'll respond as soon as possible.</p>
      <hr><br />
      <!-- <p>Got a question? We'd love to hear from you. Send us a message and we'll respond as soon as possible.</p> -->
      <form action="{{ route('contact.store') }}" method=POST >
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-row">
        <div class="form-group col-md-6">
          <label for="first_name">First Name</label>
          <input type="text" class="form-control" name="first_name" placeholder="First Name">
        </div>
        <div class="form-group col-md-6">
          <label for="last_name">Last Name</label>
          <input type="text" class="form-control" name="last_name" placeholder="Last Name">
        </div>
        </div>
        <div class="form-group">
          <label for="address">Address</label>
          <input type="text" class="form-control" name="address" placeholder="1234 Main St">
        </div>
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" name="email" placeholder="name@example.com">
        </div>
        <div class="form-group">
          <label for="message">Message</label>
          <textarea class="form-control" name="message" rows="3"></textarea>
        </div>
        <!-- <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
              Check me out
            </label>
          </div>
        </div> -->
        <button type="submit" class="btn btn-primary pull-right">Send</button>
      </form>
    </div>
    </div>
    <div class="col-md-4">
      @include('layout.partials.search-widget')
    </div>
  </div>
  </div>


  <section class="contact-forms">
   
  </section>  
  </div>
</section>
<br /><br /><br />
@endsection