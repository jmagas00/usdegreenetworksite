@extends('layout.app')

@section('main-content')
<section id="content-custom">
    <div class="container">
      	<div class="row">
        	<div class="col-lg-8">
				<div class="card my-4">
					<div class="card-header">
						<div class="row">
							<div class="col-lg-10">
								<h3 class="">{{ $schools->school_name }}</h3>
							</div>
							<div class="col-lg-2">
								<img class="logo-style" src="{{ $schools->images }}" alt="{{ $schools->school_name }}">
							</div>
						</div>
                  	</div>
					<div class="card-body">
						<form id="msform" action="{{ route('clients.store') }}" method=POST class="form-group">
							<fieldset class="panel-default">
								<div class="form-row">
									<div class="form-group col-md-12">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="school_id" value="{{ $schools->id }}">
										<input type="hidden" name="school_name" value="{{ $schools->school_name }}">
										<label for="level" style="text-align:left;"><strong>What is your highest level of education completed?</strong></label>
										<select name="level" class="form-control">
											<option>No HS Diploma/GED</option>
											<option>HS Grad</option>
											<option>GED</option>
											<option>Some College</option>
											<option>Associate's</option>
											<option>Bachelor's</option>
											<option>Master's</option>
											<option>Doctorate</option>
										</select>
									</div>
									<div class="form-group col-md-12">
										<label for="program"><strong>What program are you interested in?</strong></label>
										<select name="program" class="form-control">
										@foreach($programs as $key => $value)
											<option value="{{ $value->value }}">{{ $value->description }}</option>
										@endforeach			
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="year_graduated"><strong>Year of High School Graduation or GED completion:</strong></label>
									<input type="number" class="form-control" name="year_graduated" placeholder="">
								</div>
								<div class="form-row">
									<!-- <div class="form-group col-md-6">
										<label for="us_army_affiliation"><strong>How are you affiliated with the US military</strong></label>
										<select name="us_army_affiliation" class="form-control">
											<option value="No Affiliation">No Affiliation</option>
											<option value="Active">Active</option>
											<option value="Veteran">Veteran</option>
											<option value="Reserve">Reserve</option>	
										</select>
									</div> -->
									<div class="form-group col-md-12">
										<label for="postal_code"><strong>Please enter your postal code:</strong></label>
										<input type="text" class="form-control" name="postal_code" placeholder="">
									</div>
								</div>
								<input type="button" name="next" class="next action-button btn btn-md btn-primary col-sm-4 pull-right" value="Next"/>
							</fieldset>
							<fieldset class="panel-default">
								<div class="form-row">
									<div class="form-group col-md-12">
										<label for="country"><strong>Country:</strong></label>
										<select name="country" class="form-control">
											<option>United States</option>
											<option>Outside of U.S.</option>
										</select>
									</div>
									<div class="form-group col-md-12">
									<label for="beginning_date" style="text-align:left;"><strong>When are you interested in beginning classes?</strong></label>
										<select name="beginning_date" class="form-control">
											<option value="Immediately">Immediately</option>
											<option value="1-3 months">1-3 months</option>
											<option value="4-6 months">4-6 months</option>
											<option value="7-12 months">7-12 months</option>
											<option value="12 or more months">12 or more months</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="us_army_affiliation"><strong>How are you affiliated with the US military</strong></label>
										<select name="us_army_affiliation" class="form-control">
											<option value="No Affiliation">No Affiliation</option>
											<option value="Active">Active</option>
											<option value="Veteran">Veteran</option>
											<option value="Reserve">Reserve</option>	
										</select>
								</div>
								<div class="form-row">
									<!-- <div class="form-group col-md-6">
										<label for="us_army_affiliation"><strong>Country:</strong></label>
										<select name="us_army_affiliation" class="form-control">
											<option>United States</option>
											<option>Outside of U.S.</option>
										</select>
									</div> -->
									<div class="form-group col-md-12">
										<label for="age"><strong>AGE:</strong></label>
										<input type="number" class="form-control" name="age" placeholder="">
									</div>
								</div>
								<input type="button" name="previous" class="previous btn btn-md btn-info col-md-2" value="Back">
								<input type="button" name="next" class="next action-button btn btn-md btn-primary col-sm-4 pull-right" value="Next"/>
							</fieldset>
							<fieldset class="panel-default">
								<div class="form-row">
									<div class="form-group col-md-2">
										<label for="salutation">Salutaion</label>
										<select name="salutation" class="form-control">
											<option selected>Mr.</option>
											<option>Mrs.</option>
											<option>Dr.</option>
										</select>
									</div>
									<div class="form-group col-md-5">
										<label for="first_name">First Name</label>
										<input type="text" class="form-control" name="first_name" placeholder="">
									</div>
									<div class="form-group col-md-5">
										<label for="last_name">Last Name</label>
										<input type="text" class="form-control" name="last_name" placeholder="">
									</div>
								</div>
								<div class="form-group col-md-16">
									<label for="address">Address</label>
									<input type="text" class="form-control" name="address" placeholder="">
								</div>
								<div class="form-row">
									<div class="form-group col-md-4">
										<label for="city">City</label>
										<input type="text" class="form-control" name="city" placeholder="">
									</div>
									<div class="form-group col-md-4">
										<label for="state">State</label>
										<select name="state" class="form-control">
										@foreach($states as $key => $value)
											<option value="{{ $value->id }}">{{ $value->state }}</option>
										@endforeach			
										</select>
									</div>
									<div class="form-group col-md-4">
										<label for="zip">Zip Code</label>
										<input type="number" class="form-control" name="zip" placeholder="">
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col-md-4">
										<label for="email">Email</label>
										<input type="email" class="form-control" name="email" placeholder="">
									</div>
									<div class="form-group col-md-4">
										<label for="primary_phone">Primary Phone</label>
										<input type="number" class="form-control" name="primary_phone" placeholder="">
									</div>
									<div class="form-group col-md-4">
										<label for="secondary_phone">Secondary Phone</label>
										<input type="number" class="form-control" name="secondary_phone" placeholder="">
									</div>
								</div>
								<input type="button" name="previous" class="previous btn btn-md btn-info col-md-2" value="Back">
								<input type="submit" name="submit" class="submit btn btn-md btn-success col-md-4 pull-right" value="Request Info">
							</fieldset>
						</form>
					</div>
				</div>
			<div>
			<p>{{ $schools->school_name }} is accredited by the Accrediting Council for Independent Colleges and Schools (ACICS) to award certificates, diplomas, associate's, bachelor's, and master's degrees.</p>
			<!-- <p>For information about graduation rates, median debt of students who completed this program, and other important information, <a href="#">click here</a></p> -->
			<p>(There is no obligation or charge for information. Please only submit this form if you want to be contacted by {{ $schools->school_name }}.)</p>
			<br>
		</div>
    </div>
	<div class="col-md-4">
        <div class="card my-4">
        	<h5 class="card-header">Request No Obligation Information from</h5>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<p>To get information from this school, just answer a few simple questions and click Submit.</p>
					</div>
				</div>
			</div>
        </div>
	</div>
</section>
@endsection