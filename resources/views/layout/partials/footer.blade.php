<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white" style="font-size:12px;">Programs may vary by location and are subject to change without notice.</p>
    <p class="m-0 text-center text-white">Copyright &copy; 2018 US Degree Network | <a href="{{ url('/privacy') }}" style="color:#fff;" target="_blank">Your Privacy</a> | 
    <a href="{{ url('/contact') }}" style="color:#fff;" target="_blank">Contact Us</a> </p><br />
  </div>
</footer>