<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Search</h5>
  <div class="card-body">
    <form action="/schools/college" method="post">
      <div class="input-group">
        <input type="hidden" name="form_check" value="single">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" class="form-control" placeholder="school, university, course,..." name="search">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-secondary" type="button">Go!</button>
        </span>
      </div>
    </form><br>
    <form action="/schools/college" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="form_check" value="single">
      <label for="search">School of States</label>
        <select name="search" class="form-control" onchange="submit()">
          <option selected disabled>School State Selection</option>
				  @foreach($states as $key => $value)
					<option value="{{ $value->state }}">{{ $value->state }} Schools</option>
					@endforeach			
			  </select>
    </form>
  </div>
</div>

  <div class="card my-4">
    <h5 class="card-header">Popular School</h5>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <form action="/schools/college" method="post">            
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="form_check" value="single">
              <ul class="list-unstyled mb-0">
                 <li>
                    <a href="{{ URL::to('schools/college/Kaplan University')}}">Kaplan University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/Purdue University')}}">Purdue University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/Grand Canyon University')}}">Grand Canyon University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/Regent University')}}">Regent University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/Herzing University')}}">Herzing University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/University of Delaware')}}">University of Delaware</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/KaAmericanplan University')}}">American University</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/Baker College University')}}">Baker College Online</a>
                </li>
                <li>
                    <a href="{{ URL::to('schools/college/American InterContinental University')}}">American InterContinental University</a>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
     </div>
          <div class="card my-4">
              <h5 class="card-header">Schools by Subject</h5>
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <ul class="list-unstyled mb-0">
                        <li>
                          <a href="{{ URL::to('schools/college/Animal')}}">Agriculture and Animal Sciences</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Architectural')}}">Architectural Studies</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Design')}}">Art, Design, and Performing Arts</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Bio')}}">Biology and Biomedical Science</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Business')}}">Business Administration Programs</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Communications')}}">Communications, Journalism, and Writing</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Computer')}}">Computer Science</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Cooking')}}">Cooking and Personal Services</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Engineering')}}">Engineering</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Legal')}}">Legal Programs</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Liberal')}}">Liberal Arts, Sciences, and Humanities</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Mechanic')}}">Mechanic and Repair Technician</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/KMedical')}}">Medical and Health Programs</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Physical')}}">Physical Sciences</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Psychology')}}">Psychology Programs</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Teaching')}}">Teaching and School Administration</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
            <div class="card my-4">
                <h5 class="card-header">Narrow Your Search</h5>
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-9">
                      <ul class="list-unstyled mb-0">
                        <li>
                          <a href="{{ URL::to('schools/college/Commerce')}}">Online Commerce Coursework</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Networking')}}">Online Networking and Telecommunications Diploma Programs</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Corrections')}}">Online Corrections Associate Degrees</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Anthropology')}}">Online Anthropology Bachelor's Degree Program</a>
                        </li>
                        <li>
                          <a href="{{ URL::to('schools/college/Law')}}">Online Law Master's Program</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            <!-- <div class="card my-4">
              <h5 class="card-header">Schools by Level</h5>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-9">
                    <ul class="list-unstyled mb-0">
                      <li>
                        <a href="{{ URL::to('schools/college/Kaplan University')}}">Courses</a>
                      </li>
                      <li>
                        <a href="{{ URL::to('schools/college/Kaplan University')}}">Graduate</a>
                      </li>
                      <li>
                        <a href="{{ URL::to('schools/college/Kaplan University')}}">High School Diploma</a>
                      </li>
                      <li>
                        <a href="{{ URL::to('schools/college/Kaplan University')}}">Post-Degree Certificates</a>
                      </li>
                      <li>
                        <a href="{{ URL::to('schools/college/Kaplan University')}}">Undergrad</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> -->

            <div class="card my-4">
                <h5 class="card-header">Popular Search</h5>
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <ul class="list-unstyled mb-0">
                        <li>
                          <a href="{{ URL::to('schools/college/Computer')}}">Computer Systems Networking Undergraduate Programs in Madison, WI</a>
                        </li><br />
                        <li>
                          <a href="{{ URL::to('schools/college/Information')}}">Information Technology Associate Programs Near Baton Rouge, LA</a>
                        </li><br />
                        <li>
                          <a href="{{ URL::to('schools/college/Clinical')}}">Clinical Psychology Master's Program Near Seattle, WA</a>
                        </li><br />
                        <li>
                          <a href="{{ URL::to('schools/college/Business')}}">Business Management Undergraduate Programs Near Auburn Hills, MI</a>
                        </li><br />
                        <li>
                          <a href="{{ URL::to('schools/college/Massage')}}">Massage Therapy Diploma in Fairfield, CA</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div>
          </div>
