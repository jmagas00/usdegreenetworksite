@if ($errors->any())
    @foreach ($errors->all() as $error)
    <script>
    $.notify("{{ $error }}", "error");
    </script>
    @endforeach
@endif

@if (session('success'))
    <script>
    $.notify("{{ session('success') }}", "success");
    </script>
@endif

@if (session('warn'))
    <script>
    $.notify("{{ session('warn') }}", "warn");
    </script>
@endif

@if (session('info'))
    <script>
    $.notify("{{ session('info') }}", "info");
    </script>
@endif