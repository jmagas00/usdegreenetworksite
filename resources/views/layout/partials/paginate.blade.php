<div class="d-flex justify-content-between">
    <span id="result_count">Displaying {{ $result->currentPage() }} of {{ $result->lastPage() }} Page(s) - Total {{ $result->total() }}</span>
    <nav id="result_pagination">
        {{ $result->links() }}
    </nav>
</div>