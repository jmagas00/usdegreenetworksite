<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>US Degree Network | @yield('title', $title)</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->
	<link rel="stylesheet" href="{{ asset('css/custom-style.css') }}">


	<!-- Favicon -->
	<link rel="shortcut icon" type="image/icon" href="{{ asset('img/favicon.ico') }}"/>

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	

	<!-- Skills Circle CSS  -->
	<link rel="stylesheet" type="text/css" href="https://unpkg.com/circlebars@1.0.3/dist/circle.css">

  <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">

	<!-- Main Style -->
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/half-slider.css') }}">

  @stack('styles')
</head>

<style>


#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #677077;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
  opacity: 0.5;
  filter: alpha(opacity=50);
}

#myBtn:hover {
  background-color: #0091EB;
}
</style>
 <button onclick="topFunction()" id="myBtn" title="Go to top" class=""><i class="fa fa-arrow-circle-up fa-2x" aria-hidden="true"></i></button>