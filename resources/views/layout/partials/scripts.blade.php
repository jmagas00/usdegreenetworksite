<script src="{{ asset('js/app.js') }}"></script>

<script>

    var current_fs, next_fs, previous_fs;

    // $(".next").prop('disabled', 'disabled');
    // $(".submit").prop('disabled', 'disabled');

    $(".next").click(function(){

        // $(".next").prop('disabled', 'disabled');

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        next_fs.show(); 

        current_fs.hide();
        
    });

    $(".previous").click(function(){

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        previous_fs.show(); 

        current_fs.hide();
        
    });

    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

</script>



@stack('scripts')