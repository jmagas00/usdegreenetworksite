@extends('layout.app')

@section('main-content')
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" style="background:url('img/sliders-4-banner.jpg')">
            <div class="carousel-caption d-none d-md-block">

            </div>
          </div>
          <div class="carousel-item" style="background-image:url('img/sliders-2-banner.jpg')">
            <div class="carousel-caption d-none d-md-block">
              
            </div>
          </div>
          
          <div class="carousel-item" style="background-image:url('img/sliders-3-banner.jpg')">
            <div class="carousel-caption d-none d-md-block">
              
            </div>
          </div>
        </div>
      </div>
        <section id="headcontent">
          <div class="container">
            @if(session()->has('message'))
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                {{ session()->get('message') }}
              </div>
            @endif
          <h4><span class="heading-text"><strong>US Degree Network</strong> is an online education directory</span></h4> 
          <p style="text-align:center;">with over 1000 Colleges, Universities, and Career Schools in its database.</p> 
          <form action="schools/college" method="post">
            <input type="hidden" name="form_check" value="single">
            <input class="form-group" type="search" placeholder="Search schools, degrees, university..." value="" name="search">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn-lg btn-warning"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </section>
    </header>

        <!-- Page Content -->

    <section id="formset">
      <div class="container">
        <section class="form-header">
            <div class="container">
              <h2 style="text-align:center;">Search for Degrees and Schools</h2>
              <p style="text-align:center; font-size:13px;">You can also click on a category of interest to begin browsing through schools. From the sub-pages you can refine your search to a specific geographic location or to a specific subcategory.</p>
              <br />
             <form action="schools/college" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="form_check" value="multi">
                <div class="form-row">
                  <div class="form-group col-md-4">
                  <label for="state">State</label>
                    <select name="state" class="form-control">
										@foreach($states as $key => $value)
											<option >{{ $value->state }}</option>
										@endforeach			
										</select>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="subject">Subject - General</label>
                    <select id="subject" class="form-control">
                    @foreach($subjects as $key => $value)
											<option>{{ $value->subject_name }}</option>
										@endforeach		
                    </select>
                  </div>

                  <div class="form-group col-md-4">
                    <label for="inputDegree">Degree Level</label>
                      <select id="inputDegree" class="form-control" name="degree">
                        <optgroup label="Courses">
                            <option value="certificates and certification">Certificates and Certifications</option>
                            <option value="coursework">Coursework</option>
                            <option value="diploma program">Diploma Program</option>
                        </optgroup>
                        <optgroup label="Graduate">
                          <option value="doctorate">Doctorate</option>
                          <option value="first-professional degree">First-Professional Degree</option>
                          <option value="master">Master</option>
                        </optgroup>
                        <option style="font-weight:bold;">High School Diploma</option>
                        <optgroup label="Post-Degree Certificates">
                          <option value="first-professional certificate">First-Professional Certificate</option>
                          <option value="post-bachelor degree certificate">Post-Bachelor Degree Certificate</option>
                          <option value="post-master's certificate">Post-Master's Certificate</option>
                        </optgroup>
                        <optgroup label="Undergrad">
                          <option value="associates">Associates</option>
                          <option value="bachelors">Bachelors</option>
                        </optgroup>
                      </select>
                  </div>
                  <div class="col-md-4"></div>
                  <div class="form-row align-items-center">
                      <div class="col-auto">
                        <div class="form-check mb-2">
                          <input class="form-check-input" type="checkbox" id="autoSizingCheck">
                          <label class="form-check-label" for="autoSizingCheck">
                            Online Schools On
                          </label>
                        </div>
                      </div>
                      <div class="col-auto">
                        <!-- //  <button type="submit"  cla>SUBMIT</button> -->
                        <button type="submit" class="btn btn-primary mb-2">Search</button>
                      </div>
                  </div>
                </div>
              </form>
            </div>
          </section>
      </div>
    </section>

    <div class="container">

      <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

       <!-- Comments Form -->
          <div class="card my-4">
            <h5 class="card-header">Field of Courses</h5>
            <div class="card-body">
             <div class="row">
              <div class="col-6">
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Animal')}}" style="color:#132782;">Agriculture and Animal Sciences</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Business')}}">Agricultural Business Management,</a> 
                <a href="{{ URL::to('schools/college/Production')}}">Agricultural Production Operations,</a> 
                <a href="{{ URL::to('schools/college/Animal')}}">Animal Sciences,</a>
                <a href="{{ URL::to('schools/college/Animal')}}">Animal Services...</a></p>

                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Design')}}" style="color:#132782;">Art, Design, and Performing Arts</a></h4>
                <p style="font-size:13px;">
                <a href="{{ URL::to('schools/college/Art')}}">Art and Design,</a> 
                <a href="{{ URL::to('schools/college/Dance')}}">Dance,</a> 
                <a href="{{ URL::to('schools/college/Drama')}}">Drama and Theater,</a>
                <a href="{{ URL::to('schools/college/Video')}}">Video</a></p>
                

                <h4 style="font-size:15px;"> <a href="{{ URL::to('schools/college/Business')}}" style="color:#132782;">Business Administration</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Accounting')}}">Accounting,</a> 
                <a href="{{ URL::to('schools/college/Administrative')}}">Business Administrative Services,</a>
                <a href="{{ URL::to('schools/college/Business')}}">Business...,</a>
                <a href="{{ URL::to('schools/college/Entrepreneurship')}}">Entrepreneurship... ...</a></p>

                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Computer')}}" style="color:#132782;">Computer Science</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/System')}}">Computer and Information System Analysis,</a> 
                <a href="{{ URL::to('schools/college/System')}}">Computer Information Systems and Sciences,</a> 
                <a href="{{ URL::to('schools/college/System')}}">Computer Media...,</a>
                <a href="{{ URL::to('schools/college/Data Entry')}}">Data Entry and... ...</a></p>  
                
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Transportation')}}" style="color:#132782;">Distribution and Transportation</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Transportation')}}">Air Transportation,</a> 
                <a href="{{ URL::to('schools/college/Trucking')}}">Ground Transportation and Trucking,</a> 
                <a href="#">Maritime... ...</a></p>   
                
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Legal')}}" style="color:#132782;">Legal</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Legal')}}">Corrections,</a> 
                <a href="{{ URL::to('schools/college/Criminal')}}">Criminal Justice,</a> 
                <a href="{{ URL::to('schools/college/Law')}}">and Law Enforcement,</a>
                <a href="{{ URL::to('schools/college/Fire')}}">Fire Protection and Fire Safety,</a>
                <a href="{{ URL::to('schools/college/Criminal')}}">Professional... ...</a></p>
                
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Mechanic')}}" style="color:#132782;">Mechanic and Repair Technician</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Construction')}}">Construction,</a> 
                <a href="{{ URL::to('schools/college/Electrical')}}">Electrical Maintenance and Repair,</a> 
                <a href="{{ URL::to('schools/college/Equipment')}}">Heavy Equipment,</a>
                <a href="{{ URL::to('schools/college/Repair')}}"> HVAC Repair and...</a></p>

                <h4 style="font-size:15px;"> <a href="{{ URL::to('schools/college/Physical')}}" style="color:#132782;">Physical Sciences</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Chemistry')}}">Chemistry,</a> 
                <a href="{{ URL::to('schools/college/Fisheries')}}">Fisheries,</a> 
                <a href="{{ URL::to('schools/college/Natural')}}">Natural Resource,</a>
                <a href="{{ URL::to('schools/college/Natural')}}"> Natural Resource...</a></p>
              </div>

              <div class="col-6">
                 <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Teaching')}}" style="color:#132782;">Teaching and School Administration</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Teaching')}}">Curriculum,</a> 
                <a href="{{ URL::to('schools/college/Educational')}}">Educational Research and Assessment,</a>
                <a href="{{ URL::to('schools/college/Foundations')}}">Foundations and...,</a>
                <a href="{{ URL::to('schools/college/Foundations')}}">Int'l and... ...</a></p>


                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Architectural')}}" style="color:#132782;">Architectural Studies</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Architectural')}}">Architectural History,</a> 
                <a href="{{ URL::to('schools/college/Architectural')}}">Architectural Technologies,</a> 
               <a href="{{ URL::to('schools/college/Architectural')}}">Environmental Design,</a>
               <a href="{{ URL::to('schools/college/Architectural')}}">Interior... ...</a></p>
                
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Bio')}}" style="color:#132782;">Biology and Biomedical Science</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Bio')}}">Anatomical Sciences,</a> 
                <a href="{{ URL::to('schools/college/Bio')}}">Animal Biology,</a> 
                <a href="{{ URL::to('schools/college/Bio')}}">Biochemistry,</a>
                <a href="{{ URL::to('schools/college/Bio')}}">Biology and Sciences ...</a></p>               
                
                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Cooking')}}" style="color:#132782;">Cooking and Personal Services</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Cosmetology')}}">Cosmetology and Beauty Related Services,</a> 
                <a href="{{ URL::to('schools/college/Culinary')}}">Culinary Arts and Services,</a> 
                <a href="{{ URL::to('schools/college/Mortuary')}}">Mortuary Science... ...</a></p>   

                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Liberal')}}" style="color:#132782;">Liberal Arts, Sciences, and...</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Liberal')}}">Area and Cultural Studies,</a> 
                <a href="{{ URL::to('schools/college/Consumer')}}">Consumer Sciences,</a> 
                <a href="{{ URL::to('schools/college/Liberal')}}">Gender and Ethnic...,</a>
                <a href="{{ URL::to('schools/college/Liberal')}}">Geography and... ...</a></p>

                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Medical')}}" style="color:#132782;">Medical and Health</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Medical')}}">Alternative Medicine Professions,</a> 
                <a href="{{ URL::to('schools/college/Medical')}}">Chiropractor,</a> 
                <a href="{{ URL::to('schools/college/Dental')}}">Dental,</a>
                <a href="{{ URL::to('schools/college/Dietetics')}}">Dietetics and... ...</a></p>

                <h4 style="font-size:15px;"><a href="{{ URL::to('schools/college/Psychology')}}" style="color:#132782;">Psychology</a></h4>
                <p style="font-size:13px;"><a href="{{ URL::to('schools/college/Psychology')}}">Child Psychology,</a> 
                <a href="{{ URL::to('schools/college/Psychology')}}">Human Behavior and Psychology,</a> 
                <a href="{{ URL::to('schools/college/Psychology')}}">School Psychology... ...</a></p>  
              </div>
            </div>
          </div>
        </div>

          

          <!-- Comment with nested comments -->
          

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
         <!-- Categories Widget -->
          
          <div class="card my-4">
            <h5 class="card-header">Popular Search</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <ul class="list-unstyled mb-0">
                    <li>
                     <a href="{{ URL::to('schools/college/Woburn')}}">Networking and Telecommunications Certificate Programs Near Woburn, MA</a>
                    </li><br />
                    <li>
                     <a href="{{ URL::to('schools/college/Atlanta')}}">Medical Assistant Associate Degree in Atlanta, GA</a>
                    </li><br />
                    <li>
                      <a href="{{ URL::to('schools/college/Plantation')}}">International Business and Trade Master's Degree Programs Near Plantation, FL</a>
                    </li><br />
                    <li>
                      <a href="{{ URL::to('schools/college/Cranberry')}}">Medical Office Management Master's Program Near Cranberry Township, PA</a>
                    </li><br />
                    <li>
                     <a href="{{ URL::to('schools/college/Trevose')}}">Contract Management Bachelor's Degree Program Near Trevose, PA</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
    
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection