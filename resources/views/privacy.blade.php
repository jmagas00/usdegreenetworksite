@extends('layout.app')

@section('main-content')
<section id="content-custom">
  <div class="container">
  <header class="business-header">
    <div class="container">
       
    </div>
  </header>
  <br><br><br>
  <div class="contact-forms">
    <div class="row">
      <div class="col-md-8">
      <div class="container">

      <h1>US Degree Network Privacy Statement</h1>
        <p>We have created this privacy statement to share our commitment to your privacy.</p>
      <br>
      <h4>Personal Information and Data Collection</h4>
        <p>When requesting information from one of our partner schools you will need to fill out an online information request form so the school’s admissions and enrollment representative can follow up with you and answer your specific questions. The information you provide will be kept in strict confidence and will not be shared with any third parties other than the schools and/or their authorized agents.</p>
      <br>
      <h4>Links</h4>
        <p>US Degree Network contains links to other sites for the benefit and convenience of its visitors. Many of the sites it links to contain their own information request forms and operate under their privacy statements. US Degree Network.com is not responsible for the practices of the sites it links to.</p>
      <br>
      <h4>Cookies</h4>
        <p>US Degree Network does not use cookies to collect personal information.</p>
    </div>
    </div>
    <div class="col-md-4">
        @include('layout.partials.search-widget')
    </div>
  </div>
  </div>
<section class="contact-forms">
   
  </section>  
  </div>
</section>
<br /><br /><br />
@endsection