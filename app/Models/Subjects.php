<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    protected $table = 'subjects';

    protected $fillable = [
        'name'
    ];

    /**
     * The users that belong to the role.
     */
    public function schools()
    {
        return $this->belongsToMany('App\Models\Schools');
    }
}
