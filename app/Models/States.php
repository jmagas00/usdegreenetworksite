<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $table = 'states';

    protected $fillable = [
        'name'
    ];

    /**
     * The users that belong to the role.
     */
    public function schools()
    {
        return $this->belongsToMany('App\Models\Schools');
    }
}
