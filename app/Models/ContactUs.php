<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = 'contactus';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'subject',
        'message',
        'address'
    ];
}
