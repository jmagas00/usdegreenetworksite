<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    protected $table = 'programs';

    protected $fillable = [
       'value',
       'short_name',
       'description'
    ];
}
