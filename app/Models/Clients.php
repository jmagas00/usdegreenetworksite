<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'school_id',
        'school_name',
        'salutation',
        'first_name',
        'last_name',
        'email',
        'address',
        'city',
        'state',
        'zip',
        'age',
        'primary_phone',
        'secondary_phone',
        'best_time_to_contact',
        'subscribe',
        'country',
        'postal_code',
        'level',
        'campus',
        'year_graduated',
        'program',
        'beginning_date',
        'us_army_affiliation',
        'us_citizen',
    ];
}
