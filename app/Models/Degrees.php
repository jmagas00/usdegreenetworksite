<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Degrees extends Model
{
    protected $table = 'degrees';

    protected $fillable = [
        'name'
    ];

    /**
     * The users that belong to the role.
     */
    public function schools()
    {
        return $this->belongsToMany('App\Models\Schools');
    }
}
