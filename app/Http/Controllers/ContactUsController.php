<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactUs;
use App\Models\Subjects;
use App\Models\States;
use App\Events\SignedUp;
use View;

class ContactUsController extends Controller
{

    public function index()
    {
        $states = States::get()->all();
        $subjects = Subjects::get()->all();
        $title = 'Contact Us';

        return View::make('contact', compact('title'))
            ->with('states', $states)->with('subjects', $subjects);
    }


    public function store(Request $request)
    {
        ContactUs::create($request->all());

        return redirect('/')->with('message', 'Thankyou for contacting us!');
    }

}
