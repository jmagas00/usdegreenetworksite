<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clients;
use App\Models\Schools;
use App\Models\Programs;
use App\Models\States;

use View;

class ClientsController extends Controller
{
    public function show_school_form($id)
    {
        $schools = Schools::find($id);
        $programs = Programs::get()->all();
        $states = States::get()->all();
        $title = $schools->school_name; 

        return View::make('students.form', compact('title'))
            ->with('schools', $schools)->with('programs', $programs)->with('states', $states);
    }

    public function store(Request $request)
    {
        Clients::create($request->all());

        return redirect('/')->with('message', 'Thankyou for your Info Request!');
    }
}
