<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\States;
use App\Models\Subjects;
use View;

class PrivacyController extends Controller
{
    public function index()
    {
        $states = States::get()->all();
        $subjects = Subjects::get()->all();
        $title = 'Privacy'; 

        return View::make('privacy', compact('title'))
            ->with('states', $states)->with('subjects', $subjects);
    }
}
