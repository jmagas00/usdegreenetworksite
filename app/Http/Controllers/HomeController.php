<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clients;
use App\Models\Schools;
use App\Models\Programs;
use App\Models\States;
use App\Models\Subjects;

use View;

class HomeController extends Controller
{
    public function index()
    {
        $states = States::get()->all();
        $subjects = Subjects::get()->all();
        $title = 'Home';

        return View::make('welcome', compact('title'))
            ->with('states', $states)->with('subjects', $subjects);
    }
}
