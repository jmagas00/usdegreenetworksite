<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\state;
use App\Models\States;
use App\Models\Subjects;
use App\Models\school;
use App\Models\courses;

use View;

class LinkController extends Controller
{
    public function link_click($value)
   {    
        $search = $value;
        // $state_data = $request->input('state');
        // $subject = $request->input('subject');
        // // $degree = $request->input('degree'); 
        // $filter = $request->input('form_check');
        $states = States::get()->all();
        $subjects = Subjects::get()->all();

        
            $state = state::where('state', '=', $search)->limit(1)->first();    
            if(!empty($state)){
                $school = school::where('state_id', '=', $state->id)
                            ->where('images', '!=' , '')
                            ->paginate(10);
            }else{
                $school = school::Where('school_name', 'like', '%' .  $search  . '%')
                          ->where('images', '!=' , '')
                          ->orWhere('largest_program', 'like', '%' . $search . '%')
                           ->orWhere('address', 'like', '%' . $search . '%')
                          ->paginate(10);   
            }
        

        $title = 'search||Result'; 
        $courses  = array();
        return  view('/schools/college', compact('title'))->with('schools',$school,'courses',$courses)
                    ->with('states', $states)->with('subjects', $subjects);
        
    }


}
