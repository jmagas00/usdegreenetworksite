<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'id'            => 1,
            'state_id'      => 1,
            'state'         => 'Alabama'
        ]);
        DB::table('states')->insert([
            'id'            => 2,
            'state_id'      => 2,
            'state'         => 'Alaska'
        ]);
        DB::table('states')->insert([
            'id'            => 3,
            'state_id'      => 3,
            'state'         => 'American samoa'
        ]);
        DB::table('states')->insert([
            'id'            => 4,
            'state_id'      => 4,
            'state'         => 'Arizona'
        ]);
        DB::table('states')->insert([
            'id'            => 5,
            'state_id'      => 5,
            'state'         => 'Arkansas'
        ]);
        DB::table('states')->insert([
            'id'            => 6,
            'state_id'      => 6,
            'state'         => 'California'
        ]);
        DB::table('states')->insert([
            'id'            => 7,
            'state_id'      => 7,
            'state'         => 'Colorado'
        ]);
        DB::table('states')->insert([
            'id'            => 8,
            'state_id'      => 8,
            'state'         => 'Connecticut'
        ]);
        DB::table('states')->insert([
            'id'            => 9,
            'state_id'      => 9,
            'state'         => 'Delaware'
        ]);
        DB::table('states')->insert([
            'id'            => 10,
            'state_id'      => 10,
            'state'         => 'District of Columbia'
        ]);
        DB::table('states')->insert([
            'id'            => 11,
            'state_id'      => 11,
            'state'         => 'Fed St. Micronesia'
        ]); 
        DB::table('states')->insert([
            'id'            => 12,
            'state_id'      => 12,
            'state'         => 'Florida'
        ]);
        DB::table('states')->insert([
            'id'            => 13,
            'state_id'      => 13,
            'state'         => 'Georgia'
        ]);
        DB::table('states')->insert([
            'id'            => 14,
            'state_id'      => 14,
            'state'         => 'Guam'
        ]);
        DB::table('states')->insert([
            'id'            => 15,
            'state_id'      => 15,
            'state'         => 'Hawaii'
        ]);
        DB::table('states')->insert([
            'id'            => 16,
            'state_id'      => 16,
            'state'         => 'Idaho'
        ]);
        DB::table('states')->insert([
            'id'            => 17,
            'state_id'      => 17,
            'state'         => 'Illinois'
        ]);
        DB::table('states')->insert([
            'id'            => 18,
            'state_id'      => 18,
            'state'         => 'Indiana'
        ]);
        DB::table('states')->insert([
            'id'            => 19,
            'state_id'      => 19,
            'state'         => 'Iowa'
        ]);
        DB::table('states')->insert([
            'id'            => 20,
            'state_id'      => 20,
            'state'         => 'Kansas'
        ]);
        DB::table('states')->insert([
            'id'            => 21,
            'state_id'      => 21,
            'state'         => 'Kentucky'
        ]);
        DB::table('states')->insert([
            'id'            => 22,
            'state_id'      => 22,
            'state'         => 'Louisiana'
        ]);
        DB::table('states')->insert([
            'id'            => 23,
            'state_id'      => 23,
            'state'         => 'Maine'
        ]);
        DB::table('states')->insert([
            'id'            => 24,
            'state_id'      => 24,
            'state'         => 'Marshall Island'
        ]);
        DB::table('states')->insert([
            'id'            => 25,
            'state_id'      => 25,
            'state'         => 'Maryland'
        ]);
        DB::table('states')->insert([
            'id'            => 26,
            'state_id'      => 26,
            'state'         => 'Massachusetts'
        ]);
        DB::table('states')->insert([
            'id'            => 27,
            'state_id'      => 27,
            'state'         => 'Michigan'
        ]);
        DB::table('states')->insert([
            'id'            => 28,
            'state_id'      => 28,
            'state'         => 'Minnesota'
        ]);
        DB::table('states')->insert([
            'id'            => 29,
            'state_id'      => 29,
            'state'         => 'Mississippi'
        ]);
        DB::table('states')->insert([
            'id'            => 30,
            'state_id'      => 30,
            'state'         => 'Missouri'
        ]);
        DB::table('states')->insert([
            'id'            => 31,
            'state_id'      => 31,
            'state'         => 'Montana'
        ]);
        DB::table('states')->insert([
            'id'            => 32,
            'state_id'      => 32,
            'state'         => 'Nebraska'
        ]);
        DB::table('states')->insert([
            'id'            => 33,
            'state_id'      => 33,
            'state'         => 'Nevada'
        ]);
        DB::table('states')->insert([
            'id'            => 34,
            'state_id'      => 34,
            'state'         => 'New Hampshire'
        ]);
        DB::table('states')->insert([
            'id'            => 35,
            'state_id'      => 35,
            'state'         => 'New Jersey'
        ]);
        DB::table('states')->insert([
            'id'            => 36,
            'state_id'      => 36,
            'state'         => 'New Mexico'
        ]);
        DB::table('states')->insert([
            'id'            => 37,
            'state_id'      => 37,
            'state'         => 'New York'
        ]);
        DB::table('states')->insert([
            'id'            => 38,
            'state_id'      => 38,
            'state'         => 'North Carolina'
        ]);
        DB::table('states')->insert([
            'id'            => 39,
            'state_id'      => 39,
            'state'         => 'North Dakota'
        ]);
        DB::table('states')->insert([
            'id'            => 40,
            'state_id'      => 40,
            'state'         => 'North Marianas'
        ]);
        DB::table('states')->insert([
            'id'            => 41,
            'state_id'      => 41,
            'state'         => 'Ohio'
        ]);
        DB::table('states')->insert([
            'id'            => 42,
            'state_id'      => 42,
            'state'         => 'Oklahoma'
        ]);
        DB::table('states')->insert([
            'id'            => 43,
            'state_id'      => 43,
            'state'         => 'Oregon'
        ]);
        DB::table('states')->insert([
            'id'            => 44,
            'state_id'      => 44,
            'state'         => 'Palau'
        ]);
        DB::table('states')->insert([
            'id'            => 45,
            'state_id'      => 45,
            'state'         => 'Pennsylvania'
        ]);
        DB::table('states')->insert([
            'id'            => 46,
            'state_id'      => 46,
            'state'         => 'Puerto Rico'
        ]);
        DB::table('states')->insert([
            'id'            => 47,
            'state_id'      => 47,
            'state'         => 'Rhode Island'
        ]);
        DB::table('states')->insert([
            'id'            => 48,
            'state_id'      => 48,
            'state'         => 'South Carolina'
        ]);
        DB::table('states')->insert([
            'id'            => 49,
            'state_id'      => 49,
            'state'         => 'South Dakota'
        ]);
        DB::table('states')->insert([
            'id'            => 50,
            'state_id'      => 50,
            'state'         => 'Tennessee'
        ]);
        DB::table('states')->insert([
            'id'            => 51,
            'state_id'      => 51,
            'state'         => 'Texas'
        ]);  
        DB::table('states')->insert([
            'id'            => 52,
            'state_id'      => 52,
            'state'         => 'Utah'
        ]); 
        DB::table('states')->insert([
            'id'            => 53,
            'state_id'      => 53,
            'state'         => 'Vermont'
        ]); 
        DB::table('states')->insert([
            'id'            => 54,
            'state_id'      => 54,
            'state'         => 'Virgin Islands'
        ]); 
        DB::table('states')->insert([
            'id'            => 55,
            'state_id'      => 55,
            'state'         => 'Virginia'
        ]); 
        DB::table('states')->insert([
            'id'            => 56,
            'state_id'      => 56,
            'state'         => 'Washington'
        ]); 
        DB::table('states')->insert([
            'id'            => 57,
            'state_id'      => 57,
            'state'         => 'West Virginia'
        ]);    
        DB::table('states')->insert([
            'id'            => 58,
            'state_id'      => 58,
            'state'         => 'Wisconsin'
        ]); 
        DB::table('states')->insert([
            'id'            => 59,
            'state_id'      => 59,
            'state'         => 'Wyoming'
        ]); 
    }
}
