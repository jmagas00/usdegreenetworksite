<?php

use Illuminate\Database\Seeder;

class DegreesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('degrees')->insert([
            'id'            => 1,
            'degree_name'   => 'Degree Name'
        ]);
    }
}
