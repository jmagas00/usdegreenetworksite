<?php

use Illuminate\Database\Seeder;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'id'            => 1,
            'subject_name'  => 'Agriculture and Animal Sciences'
        ]);
        DB::table('subjects')->insert([
            'id'            => 2,
            'subject_name'  => 'Architectural Studies'
        ]);
        DB::table('subjects')->insert([
            'id'            => 3,
            'subject_name'  => 'Art, Design, and Performing Arts'
        ]);
        DB::table('subjects')->insert([
            'id'            => 4,
            'subject_name'  => 'Biology and Biomedical Science'
        ]);
        DB::table('subjects')->insert([
            'id'            => 5,
            'subject_name'  => 'Business Administration'
        ]);
        DB::table('subjects')->insert([
            'id'            => 6,
            'subject_name'  => 'Communications, Journalism, and Writing'
        ]);
        DB::table('subjects')->insert([
            'id'            => 7,
            'subject_name'  => 'Computer Science'
        ]);
        DB::table('subjects')->insert([
            'id'            => 8,
            'subject_name'  => 'Cooking and Personal Services'
        ]);
        DB::table('subjects')->insert([
            'id'            => 9,
            'subject_name'  => 'Distribution and Transportation'
        ]);
        DB::table('subjects')->insert([
            'id'            => 10,
            'subject_name'  => 'Engineering'
        ]);
        DB::table('subjects')->insert([
            'id'            => 11,
            'subject_name'  => 'Legal'
        ]);
        DB::table('subjects')->insert([
            'id'            => 12,
            'subject_name'  => 'Liberal Arts, Sciences, and Humanities'
        ]);
        DB::table('subjects')->insert([
            'id'            => 13,
            'subject_name'  => 'Mechanic and Repair Technician'
        ]);
        DB::table('subjects')->insert([
            'id'            => 14,
            'subject_name'  => 'Medical and Health'
        ]);
        DB::table('subjects')->insert([
            'id'            => 15,
            'subject_name'  => 'Physical Sciences'
        ]);
        DB::table('subjects')->insert([
            'id'            => 16,
            'subject_name'  => 'Psychology'
        ]);
        DB::table('subjects')->insert([
            'id'            => 17,
            'subject_name'  => 'Teaching and School Administration'
        ]);
    }
}
