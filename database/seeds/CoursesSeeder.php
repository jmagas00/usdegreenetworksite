<?php

use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            'id'            => 1,
            'school_id'     => '3492',
            'course_name'   => 'Medical Assistant'
        ]);
        DB::table('courses')->insert([
            'id'            => 2,
            'school_id'         => '3492',
            'course_name'    => 'Physical Education and Fitness'
        ]);
        DB::table('courses')->insert([
            'id'            => 3,
            'school_id'         => '3492',
            'course_name'    => 'Public Health and Public Safety'
        ]);
        DB::table('courses')->insert([
            'id'            => 4,
            'school_id'         => '3492',
            'course_name'    => 'Social and Mental Health Services'
        ]);
        DB::table('courses')->insert([
            'id'            => 5,
            'school_id'         => '3474',
            'course_name'    => 'Art and Design'
        ]);
        DB::table('courses')->insert([
            'id'            => 6,
            'school_id'         => '3474',
            'course_name'    => 'Dance'
        ]);
        DB::table('courses')->insert([
            'id'            => 7,
            'school_id'         => '3474',
            'course_name'    => 'Drama and Theatre'
        ]);
        DB::table('courses')->insert([
            'id'            => 8,
            'school_id'         => '3474',
            'course_name'    => 'Fine Art and Studio Art'
        ]);
        DB::table('courses')->insert([
            'id'            => 9,
            'school_id'         => '4431',
            'course_name'    => 'Master of Public Administration'
        ]);
        DB::table('courses')->insert([
            'id'            => 10,
            'school_id'         => '4431',
            'course_name'    => 'Master of Business Administration'
        ]);
        DB::table('courses')->insert([
            'id'            => 11,
            'school_id'         => '4431',
            'course_name'    => 'Master of Business Administration - Finance'
        ]);
        DB::table('courses')->insert([
            'id'            => 12,
            'school_id'         => '4431',
            'course_name'    => 'Master of Business Administration - Healthcare Concentration'
        ]);
        DB::table('courses')->insert([
            'id'            => 13,
            'school_id'         => '4431',
            'course_name'    => 'Master of Business Administration - International Business'
        ]);
        DB::table('courses')->insert([
            'id'            => 14,
            'school_id'         => '4431',
            'course_name'    => 'Master of Education in Teacher Leadership'
        ]);
    }
}
