<?php

use Illuminate\Database\Seeder;

class ProgramsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            'id'            => 1,
            'value'         => 'agriculture',
            'short_name'    => 'Agriculture',
            'description'   => 'Agriculture, Agriculture Operations, and Related Sciences'
        ]);
        DB::table('programs')->insert([
            'id'            => 2,
            'value'         => 'architecture',
            'short_name'    => 'Architecture',
            'description'   => 'Architecture and Related Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 3,
            'value'         => 'ethnic_cultural_gender',
            'short_name'    => 'Ethnic Cultural Gender',
            'description'   => 'Biological and Biomedical Sciences'
        ]);
        DB::table('programs')->insert([
            'id'            => 4,
            'value'         => 'business_marketing',
            'short_name'    => 'Business Marketing',
            'description'   => 'Business, Management, Marketing, and Related Support Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 5,
            'value'         => 'communication',
            'short_name'    => 'Communication',
            'description'   => 'Communication, Journalism, and Related Programs'
        ]);
        DB::table('programs')->insert([
            'id'            => 6,
            'value'         => 'communications_technology',
            'short_name'    => 'Communications Technology',
            'description'   => 'Communications Technologies/Technicians and Support Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 7,
            'value'         => 'computer',
            'short_name'    => 'Computer',
            'description'   => 'Computer and Information Sciences and Support Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 8,
            'value'         => 'construction',
            'short_name'    => 'Construction',
            'description'   => 'Construction Trades'
        ]);
        DB::table('programs')->insert([
            'id'            => 9,
            'value'         => 'education',
            'short_name'    => 'Education',
            'description'   => 'Education'
        ]);
        DB::table('programs')->insert([
            'id'            => 10,
            'value'         => 'engineering',
            'short_name'    => 'Engineering',
            'description'   => 'Engineering'
        ]);
        DB::table('programs')->insert([
            'id'            => 11,
            'value'         => 'engineering_technology',
            'short_name'    => 'Engineering Technology',
            'description'   => 'Engineering Technologies and Engineering-Related Fields'
        ]);
        DB::table('programs')->insert([
            'id'            => 12,
            'value'         => 'english',
            'short_name'    => 'English',
            'description'   => 'English Language and Literature/Letters'
        ]);
        DB::table('programs')->insert([
            'id'            => 13,
            'value'         => 'family_consumer_science',
            'short_name'    => 'Family Consumer Science',
            'description'   => 'Family and Consumer Sciences/Human Sciences'
        ]);
        DB::table('programs')->insert([
            'id'            => 14,
            'value'         => 'language',
            'short_name'    => 'Language',
            'description'   => 'Foreign Languages, Literatures, and Linguistics'
        ]);
        DB::table('programs')->insert([
            'id'            => 15,
            'value'         => 'health',
            'short_name'    => 'Health',
            'description'   => 'Health Professions and Related Programs'
        ]);
        DB::table('programs')->insert([
            'id'            => 16,
            'value'         => 'history',
            'short_name'    => 'History',
            'description'   => 'History'
        ]);
        DB::table('programs')->insert([
            'id'            => 17,
            'value'         => 'security_law_enforcement',
            'short_name'    => 'Security Law Enforcement',
            'description'   => 'Homeland Security, Law Enforcement, Firefighting and Related Protective Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 18,
            'value'         => 'legal',
            'short_name'    => 'Legal',
            'description'   => 'Legal Professions and Studies'
        ]);
        DB::table('programs')->insert([
            'id'            => 19,
            'value'         => 'humanities',
            'short_name'    => 'Humanities',
            'description'   => 'Liberal Arts and Sciences, General Studies and Humanities'
        ]);
        DB::table('programs')->insert([
            'id'            => 20,
            'value'         => 'library',
            'short_name'    => 'Library',
            'description'   => 'Library Science'
        ]);
        DB::table('programs')->insert([
            'id'            => 21,
            'value'         => 'mathematics',
            'short_name'    => 'Mathematics',
            'description'   => 'Mathematics and Statistics'
        ]);
        DB::table('programs')->insert([
            'id'            => 22,
            'value'         => 'mechanic_repair_technology',
            'short_name'    => 'Mechanic Repair Technology',
            'description'   => 'Mechanic and Repair Technologies/Technicians'
        ]);
        DB::table('programs')->insert([
            'id'            => 23,
            'value'         => 'military',
            'short_name'    => 'Military',
            'description'   => 'Military Technologies and Applied Sciences'
        ]);
        DB::table('programs')->insert([
            'id'            => 24,
            'value'         => 'multidiscipline',
            'short_name'    => 'Multidiscipline',
            'description'   => 'Multi/Interdisciplinary Studies'
        ]);
        DB::table('programs')->insert([
            'id'            => 25,
            'value'         => 'resources',
            'short_name'    => 'Resources',
            'description'   => 'Natural Resources and Conservation'
        ]);
        DB::table('programs')->insert([
            'id'            => 26,
            'value'         => 'parks_recreation_fitness',
            'short_name'    => 'Parks Recreation Fitness',
            'description'   => 'Parks, Recreation, Leisure, and Fitness Studies'
        ]);
        DB::table('programs')->insert([
            'id'            => 27,
            'value'         => 'personal_culinary',
            'short_name'    => 'Personal Culinary',
            'description'   => 'Personal and Culinary Services'
        ]);
        DB::table('programs')->insert([
            'id'            => 28,
            'value'         => 'philosophy_religious',
            'short_name'    => 'Philosophy Religious',
            'description'   => 'Philosophy and Religious Studies'
        ]);
        DB::table('programs')->insert([
            'id'            => 29,
            'value'         => 'physical_science',
            'short_name'    => 'Physical Science',
            'description'   => 'Physical Science'
        ]);
        DB::table('programs')->insert([
            'id'            => 30,
            'value'         => 'precision_production',
            'short_name'    => 'Precision Production',
            'description'   => 'Precision Production'
        ]);
        DB::table('programs')->insert([
            'id'            => 31,
            'value'         => 'psychology',
            'short_name'    => 'Psychology',
            'description'   => 'Psychology'
        ]);
        DB::table('programs')->insert([
            'id'            => 32,
            'value'         => 'public_administration_social_service',
            'short_name'    => 'Public Administration Social Service',
            'description'   => 'Public Administration and Social Service Professions'
        ]);
        DB::table('programs')->insert([
            'id'            => 33,
            'value'         => 'science_technology',
            'short_name'    => 'Science Technology',
            'description'   => 'Science Technologies/Technicians'
        ]);
        DB::table('programs')->insert([
            'id'            => 34,
            'value'         => 'social_science',
            'short_name'    => 'Social Science',
            'description'   => 'Social Sciences'
        ]);
        DB::table('programs')->insert([
            'id'            => 35,
            'value'         => 'theology_religious_vocation',
            'short_name'    => 'Theology Religious Vocation',
            'description'   => 'Theology and Religious Vocations'
        ]);
        DB::table('programs')->insert([
            'id'            => 36,
            'value'         => 'transportation',
            'short_name'    => 'Transportation',
            'description'   => 'Transportation and Materials Moving'
        ]);
        DB::table('programs')->insert([
            'id'            => 37,
            'value'         => 'visual_performing',
            'short_name'    => 'Visual Performing',
            'description'   => 'Visual and Performing Arts'
        ]);
    }
}
