<?php

use Illuminate\Database\Seeder;

class SchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert([
            'id'            => 1,
            'school_name'   => '',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 2,
            'school_name'   => 'Kaplan University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 3,
            'school_name'   => 'Full Sail University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 4,
            'school_name'   => 'American InterContinental University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 5,
            'school_name'   => 'Grand Canyon University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 6,
            'school_name'   => 'Purdue University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 7,
            'school_name'   => 'Baker College Online',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 8,
            'school_name'   => 'Herzing University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 9,
            'school_name'   => 'Concordia University Portland',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 10,
            'school_name'   => 'Georgetown University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 11,
            'school_name'   => 'Virginia College',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 12,
            'school_name'   => 'Seton Hall University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 13,
            'school_name'   => 'Saint Mary\'s University of Minnesota',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 14,
            'school_name'   => 'Colorado Technical University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 15,
            'school_name'   => 'Northcentral University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 16,
            'school_name'   => 'University of Saint Mary',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 17,
            'school_name'   => 'Colorado State University Global',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 18,
            'school_name'   => 'Lewis University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 19,
            'school_name'   => 'Johns Hopkins University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 20,
            'school_name'   => 'Keiser University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 21,
            'school_name'   => 'American University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 22,
            'school_name'   => 'George Mason University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 23,
            'school_name'   => 'Saint Joseph\'s University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 24,
            'school_name'   => 'Widener University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 25,
            'school_name'   => 'Sacred Heart University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 26,
            'school_name'   => 'University of Delaware',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 27,
            'school_name'   => 'The University of Scranton',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 28,
            'school_name'   => 'Penn Foster High School',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 29,
            'school_name'   => 'The George Washington University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 30,
            'school_name'   => 'Brightwood College',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 31,
            'school_name'   => 'Universal Techinical Institute',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 32,
            'school_name'   => 'Queens University of Charlotte',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 33,
            'school_name'   => 'Saint John\'s University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 34,
            'school_name'   => 'Utica College',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 35,
            'school_name'   => 'Saint Leo University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 36,
            'school_name'   => 'Penn Foster Career School',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 37,
            'school_name'   => 'Cortiva Institute',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 38,
            'school_name'   => 'Central Christian College of Kansas',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 39,
            'school_name'   => 'Fortis College',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 40,
            'school_name'   => 'Regent University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 41,
            'school_name'   => 'Indiana Wesleyan University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 42,
            'school_name'   => 'American National University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 43,
            'school_name'   => 'Benedictine University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 44,
            'school_name'   => 'Abilene Christian University',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 45,
            'school_name'   => 'The University of Alabama',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
        DB::table('schools')->insert([
            'id'            => 46,
            'school_name'   => 'CDI College',
            'subject_id'    => 1,
            'degree_id'     => 1,
            'state_id'      => 1,
            'level_id'      => 1
        ]);
    }
}
