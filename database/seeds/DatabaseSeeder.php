<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SchoolsSeeder::class);
        $this->call(SubjectsSeeder::class);
        $this->call(DegreesSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(LevelsSeeder::class);
        $this->call(ProgramsSeeder::class);
        $this->call(CoursesSeeder::class);
    }
}
