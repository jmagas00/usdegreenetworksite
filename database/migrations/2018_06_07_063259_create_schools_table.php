<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->index()->nullable();
            $table->integer('subject_id')->index()->nullable();
            $table->integer('state_id')->index()->nullable();
            $table->integer('level_id')->index()->nullable();
            $table->string('state_name')->nullable();
            $table->string('school_name')->nullable();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('largest_program')->nullable();
            $table->string('images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
