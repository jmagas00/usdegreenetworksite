<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->index()->nullable();  
            $table->string('school_name')->nullable();
            $table->string('salutation')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->integer('age')->nullable();
            $table->string('primary_phone')->nullable();
            $table->string('secondary_phone')->nullable();
            $table->string('best_time_to_contact')->nullable();
            $table->string('subscribe')->nullable();
            $table->string('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('level')->nullable();
            $table->string('campus')->nullable();
            $table->integer('year_graduated')->nullable();
            $table->string('program')->nullable();
            $table->string('beginning_date')->nullable();
            $table->string('us_army_affiliation')->nullable();
            $table->string('us_citizen')->nullable();
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
