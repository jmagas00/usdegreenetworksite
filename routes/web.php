<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $title = 'Home';
    return view('welcome', compact('title'));
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::get('/welcome', 'HomeController@index');

Route::get('privacy', 'PrivacyController@index')->name('privacy');

Route::get('contact', 'ContactUsController@index')->name('contact');
Route::post('contact', 'ContactUsController@store')->name('contact.store');

//admin
Route::get('/administrator', function () {
    $title = 'Administrator'; 
    return view('administrator', compact('title'));
});
Route::post('clients', 'ClientsController@store')->name('clients.store');


Route::get('schools/college', 'DateController@getresult');
Route::post('schools/college', 'DateController@getresult');
//alabama
// Route::get('/schools/college', function () {
//     $title = 'School Name'; 
//     return view('schools/college', compact('title'));
// });


Route::get('/students/form/{id}', 'ClientsController@show_school_form');


Route::get('schools/college/{value}', 'LinkController@link_click');


//student
Route::get('/students/form', function () {
    $title = 'Student Form'; 
    return view('students/form', compact('title'));
});

//categories
Route::get('/categories/Agriculture_and_Animal_Sciences', function () {
    $title = 'Agriculture and Animal Sciences'; 
    return view('categories/Agriculture_and_Animal_Sciences', compact('title'));
});

//footer
// Route::get('/contact', function () {
//     $title = 'Contact Us'; 
//     return view('contact', compact('title'));
// });
// Route::get('/privacy', function () {
//     $title = 'Privacy'; 
//     return view('privacy', compact('title'));
// });